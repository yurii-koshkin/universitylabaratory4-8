package org.database;

import org.gui.CompanyIdReference;
import org.main.company.Company;
import org.main.order.Order;
import org.main.tariff.Tariff;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class DBController {
    private static final String CREATE_COMPANY_QUERY = "INSERT INTO company (company_name) VALUES (?)";
    private static final String GET_COMPANIES_QUERY = "SELECT * FROM company";
    private static final String DELETE_COMPANY_QUERY = "DELETE FROM company WHERE company_id = (?)";
    private static final String CREATE_ORDER_QUERY = "INSERT INTO \"order\" (tariff_name, add_service_name, internet_gb, add_internet_gb, calls, add_calls, price, company_id, start_date, end_date) VALUES (?,?,?,?,?,?,?,?,?,?)";
    private static final String GET_ORDERS_QUERY = "SELECT * FROM \"order\" WHERE company_id=(?)";
    private static final String DELETE_ORDER_QUERY = "DELETE FROM \"order\" WHERE order_id = (?)";

    private static Connection connection = null;

    private static void connect() {
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "32145656");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void createCompany(String name) {
        try {
            connect();
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_COMPANY_QUERY);
            preparedStatement.setString(1, name);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, Integer> getCompanies() {
        try {
            connect();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_COMPANIES_QUERY);
            ResultSet resultSet = preparedStatement.executeQuery();

            Map<String, Integer> companies = new HashMap<>();

            while (resultSet.next()) {
                companies.put(resultSet.getString("company_name") + " (Id=" + resultSet.getInt("company_id") + ")", resultSet.getInt("company_id"));
            }

            connection.close();
            preparedStatement.close();

            return companies;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void deleteCompany(int company_id) {
        try {
            connect();

            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_COMPANY_QUERY);
            preparedStatement.setInt(1, company_id);
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void createOrder(Order order) {
        try {
            connect();

            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_ORDER_QUERY);
            preparedStatement.setString(1, order.getTariff().getName());
            preparedStatement.setString(2, order.getAddServiceName());
            preparedStatement.setDouble(3, order.getTariff().getInternetGigabytes());
            preparedStatement.setDouble(4, order.getTariff().getAdditionalInternetGigabytes());
            preparedStatement.setInt(5, order.getTariff().getCallMinutes());
            preparedStatement.setInt(6, order.getTariff().getAdditionalCallMinutes());
            preparedStatement.setDouble(7, order.getPrice());
            preparedStatement.setInt(8, CompanyIdReference.getCompanyId());
            preparedStatement.setDate(9, new java.sql.Date(order.getStartDate().getTime()));
            preparedStatement.setDate(10, new java.sql.Date(order.getEndDate().getTime()));
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, Integer> getOrders() {
        try {
            connect();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_ORDERS_QUERY);
            preparedStatement.setInt(1, CompanyIdReference.getCompanyId());
            ResultSet resultSet = preparedStatement.executeQuery();

            Map<String, Integer> orders = new HashMap<>();

            while (resultSet.next()) {
                orders.put(resultSet.getString("tariff_name") + " (Id=" + resultSet.getInt("order_id") + ")", resultSet.getInt("order_id"));
            }

            connection.close();
            preparedStatement.close();

            return orders;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void deleteOrder(int order_id) {
        try {
            connect();

            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ORDER_QUERY);
            preparedStatement.setInt(1, order_id);
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static Map<String, Order> getDetailOrders() {
        try {
            connect();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_ORDERS_QUERY);
            preparedStatement.setInt(1, CompanyIdReference.getCompanyId());
            ResultSet resultSet = preparedStatement.executeQuery();

            Map<String, Order> orders = new HashMap<>();

            while (resultSet.next()) {
                orders.put(resultSet.getString("tariff_name") + " (Id=" + resultSet.getInt("order_id") + ")", formOrder(resultSet));
            }

            connection.close();
            preparedStatement.close();

            return orders;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static Order formOrder(ResultSet resultSet) throws SQLException {
        Map<String, Tariff> tariffs = new Company().getTariffs();

        Order order = new Order();
        order.setTariff(tariffs.get((resultSet.getString("tariff_name"))));
        order.getTariff().setName((resultSet.getString("tariff_name")));
        order.setAddServiceName(resultSet.getString("add_service_name"));
        order.getTariff().addInternetGigabytes(resultSet.getDouble("add_internet_gb"));
        order.getTariff().addCallMinutes(resultSet.getInt("add_calls"));
        order.setStartDate(resultSet.getDate("start_date"));
        order.setEndDate(resultSet.getDate("end_date"));

        return order;
    }

}
