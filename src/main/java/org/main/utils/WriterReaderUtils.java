package org.main.utils;

import org.main.company.Company;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class WriterReaderUtils {

    public static void exportCompany(Company company, String fileName) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(Files.newOutputStream(Paths.get(fileName + ".txt")))) {
            objectOutputStream.writeObject(company);
        } catch (IOException e) {
            ApplicationLogger.logFatal(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public static Company importCompany(String fileName) {
        Company company;

        try (ObjectInputStream objectInputStream = new ObjectInputStream(Files.newInputStream(Paths.get(fileName + ".txt")))) {
            company = (Company) objectInputStream.readObject();
        } catch (Exception e) {
            ApplicationLogger.logFatal(e.getMessage());
            throw new RuntimeException(e);
        }

        return company;
    }

}
