package org.main.utils;

import org.main.additionalservices.AdditionalService;
import org.main.additionalservices.HomeInternet;
import org.main.additionalservices.HomeTV;
import org.main.additionalservices.HomeUltimate;

public class AdditionalServiceUtils {

    public static AdditionalService getAdditionalServiceByName(String name) {
        switch (name.toLowerCase()) {
            case "home_tv" -> {
                return new HomeTV();
            }
            case "home_internet" -> {
                return new HomeInternet();
            }
            case "home_ultimate" -> {
                return new HomeUltimate();
            }
            default -> {
                return null;
            }
        }
    }

}
