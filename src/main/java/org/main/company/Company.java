package org.main.company;

import org.main.order.Order;
import org.main.tariff.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Company implements Serializable {
    private static final long  SerialVersionUID = 13L;

    private List<Order> orders = new ArrayList<>();
    private final Map<String, Tariff> tariffs = new HashMap<>();

    public Company() {
        this.tariffs.put("mega_calls", new MegaCalls());
        this.tariffs.put("mega_calls_starter", new MegaCallsStarter());
        this.tariffs.put("mega_calls_premium", new MegaCallsPremium());
        this.tariffs.put("internet_addicted", new InternetAddicted());
        this.tariffs.put("internet_addicted_started", new InternetAddictedStarter());
        this.tariffs.put("internet_addicted_premium", new InternetAddictedPremium());
    }

    public void importCompany(Company company) {
        this.orders = company.orders;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public int getCustomersCount() {
        return this.orders.size();
    }

    public Map<String, Tariff> getTariffs() { return tariffs; }

    public void addOrder(Order order) {
        this.orders.add(order);
    }

}
