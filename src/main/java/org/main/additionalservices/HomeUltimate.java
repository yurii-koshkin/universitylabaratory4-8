package org.main.additionalservices;

import java.io.Serializable;

public class HomeUltimate extends AdditionalService implements Serializable {
    private static final long  SerialVersionUID = 2L;

    public HomeUltimate() {
        super( 100, 60, 150);
    }

    @Override
    public String toString() {
        return "Home Internet and TV " + super.toString();
    }
}
