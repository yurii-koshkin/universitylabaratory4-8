package org.main.additionalservices;

import java.io.Serializable;

public class HomeTV extends AdditionalService implements Serializable {
    private static final long  SerialVersionUID = 4L;

    public HomeTV() {
        super(0, 60, 100);
    }

    @Override
    public String toString() {
        return "Home TV " + super.toString();
    }

}
