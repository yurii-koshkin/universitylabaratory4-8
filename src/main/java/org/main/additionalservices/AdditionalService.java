package org.main.additionalservices;

import java.io.Serializable;

public abstract class AdditionalService implements Serializable {
    private static final long  SerialVersionUID = 1L;

    protected int connectionSpeed;
    protected int channelsCount;
    protected double price;

    protected AdditionalService(int connectionSpeed, int channelsCount, double price) {
        this.connectionSpeed = connectionSpeed;
        this.channelsCount = channelsCount;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public int getConnectionSpeed() {
        return connectionSpeed;
    }

    public int getChannelsCount() {
        return channelsCount;
    }

    @Override
    public String toString() {
        return  String.format("(Speed: %d, Channels: %d): %f UAH", this.connectionSpeed, this.channelsCount, this.price);
    }

}
