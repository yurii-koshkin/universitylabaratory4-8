package org.main.additionalservices;

import java.io.Serializable;

public class HomeInternet extends AdditionalService implements Serializable {
    private static final long  SerialVersionUID = 3L;

    public HomeInternet() {
        super(100, 0, 100);
    }

    @Override
    public String toString() {
        return "Home Internet " + super.toString();
    }
}
