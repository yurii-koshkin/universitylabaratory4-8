package org.main.menu;

import org.main.company.Company;

import java.util.List;

public class PrintOrdersCommand implements Command {
    private final Company company;

    public PrintOrdersCommand(Company company) {
        this.company = company;
    }

    @Override
    public String getKey() {
        return "print_orders";
    }

    @Override
    public void execute(List<String> parameters) {
        System.out.println("Company orders:");
        company.getOrders().forEach(t -> System.out.println("\n" + t));
    }

}
