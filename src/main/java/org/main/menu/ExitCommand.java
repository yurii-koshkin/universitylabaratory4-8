package org.main.menu;

import org.main.utils.ApplicationLogger;

import java.util.List;

public class ExitCommand implements Command {
    @Override
    public String getKey() {
        return "exit";
    }

    @Override
    public void execute(List<String> parameters) {
        System.out.println("See you soon");
        ApplicationLogger.logInfo(this.getClass(), "System shutdown");
        System.exit(0);
    }
}
