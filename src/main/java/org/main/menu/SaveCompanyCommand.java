package org.main.menu;

import org.main.company.Company;
import org.main.utils.WriterReaderUtils;

import java.util.List;

public class SaveCompanyCommand implements Command {
    private final Company company;

    public SaveCompanyCommand(Company company) {
        this.company = company;
    }

    @Override
    public String getKey() {
        return "save_company";
    }

    @Override
    public String getParameters() {
        return "[file name]";
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() > 0) {
            WriterReaderUtils.exportCompany(this.company, String.join(" ", parameters));
            System.out.println("Company was successfully saved!");
        } else {
            System.out.println("Wrong input!");
        }
    }

}
