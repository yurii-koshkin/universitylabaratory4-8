package org.main.menu;

import java.util.List;

public interface Command {
    void execute(List<String> parameters);

    String getKey();

    default String getParameters() {
        return "";
    }
}
