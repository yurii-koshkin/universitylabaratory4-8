package org.main.menu;

import org.main.company.Company;
import org.main.menu.submenu.SubMenu;
import org.main.order.Order;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class OpenSubMenuCommand implements Command {
    private final SubMenu subMenu;

    public OpenSubMenuCommand(Company company) {
        subMenu = new SubMenu(company, new Order());
    }

    @Override
    public String getKey() {
        return "create_order";
    }

    @Override
    public void execute(List<String> parameters) {
        List<String> commandWithParams;
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.print("\n(Order) Input your command: ");
            commandWithParams = new ArrayList<>(Arrays.asList(scanner.nextLine().split(" ")));
            subMenu.execute(commandWithParams);
        } while (!commandWithParams.get(0).equals("complete"));
    }

}
