package org.main.menu;

import org.main.company.Company;

import java.util.List;

public class PrintCustomersCountCommand implements Command {
    private final Company company;

    public PrintCustomersCountCommand(Company company) {
        this.company = company;
    }

    @Override
    public String getKey() {
        return "print_customers_count";
    }

    @Override
    public void execute(List<String> parameters) {
        System.out.println(company.getCustomersCount());
    }
}
