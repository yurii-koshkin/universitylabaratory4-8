package org.main.menu.submenu;

import org.main.menu.Command;
import org.main.order.Order;

import java.util.List;

public class AddCallsCommand implements Command {
    private final Order order;

    public AddCallsCommand(Order order) {
        this.order = order;
    }

    @Override
    public String getKey() {
        return "add_call_minutes";
    }

    @Override
    public String getParameters() {
        return "[minutes]";
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() == 1) {
            if (this.order.getTariff() != null) {
                this.order.getTariff().addCallMinutes(Integer.parseInt(parameters.get(0)));
                System.out.println(parameters.get(0) + " call minutes was successfully added to your tariff");
            } else {
                System.out.println("You should firstly choose tariff");
            }
        } else {
            System.out.println("Wrong Input!");
        }
    }

}
