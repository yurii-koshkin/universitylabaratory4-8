package org.main.menu.submenu;

import org.main.menu.Command;
import org.main.order.Order;

import java.util.List;

public class PrintOrderCommand implements Command {
    private final Order order;

    public PrintOrderCommand(Order order) {
        this.order = order;
    }

    @Override
    public String getKey() {
        return "print_order";
    }

    @Override
    public void execute(List<String> parameters) {
        System.out.println("Your order:\n\n" + this.order);
    }

}
