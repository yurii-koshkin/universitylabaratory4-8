package org.main.menu.submenu;

import org.main.menu.Command;
import org.main.order.Order;

import java.util.List;

public class AddGigabytesCommand implements Command {
    private final Order order;

    public AddGigabytesCommand(Order order) {
        this.order = order;
    }

    @Override
    public String getKey() {
        return "add_gigabytes";
    }

    @Override
    public String getParameters() {
        return "[gb count]";
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() == 1) {
            if (this.order.getTariff() != null) {
                this.order.getTariff().addInternetGigabytes(Double.parseDouble(parameters.get(0)));
                System.out.println(parameters.get(0) + " gb was successfully added to your tariff");
            } else {
                System.out.println("You should firstly choose tariff");
            }
        } else {
            System.out.println("Wrong Input!");
        }
    }

}
