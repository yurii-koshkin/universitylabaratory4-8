package org.main.menu.submenu;

import org.main.company.Company;
import org.main.menu.Command;
import org.main.menu.Menu;
import org.main.order.Order;

public class SubMenu extends Menu {
    private final Company company;
    private final Order order;

    public SubMenu(Company company, Order order) {
        this.company = company;
        this.order = order;
        initMenu();
    }

    @Override
    protected void initMenu() {
        Command command = new PrintOrderCommand(order);
        super.commands.put(command.getKey(), command);

        command = new ChooseTariffCommand(company, order);
        super.commands.put(command.getKey(), command);

        command = new CompleteOrderCommand(company, order);
        super.commands.put(command.getKey(), command);

        command = new ChooseAdditionalServiceCommand(order);
        super.commands.put(command.getKey(), command);

        command = new RemoveAdditionalServiceCommand(order);
        super.commands.put(command.getKey(), command);

        command = new AddGigabytesCommand(order);
        super.commands.put(command.getKey(), command);

        command = new RemoveAdditionalGigabytesCommand(order);
        super.commands.put(command.getKey(), command);

        command = new AddCallsCommand(order);
        super.commands.put(command.getKey(), command);

        command = new RemoveCallsCommand(order);
        super.commands.put(command.getKey(), command);
    }

}
