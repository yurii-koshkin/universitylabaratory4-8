package org.main.menu.submenu;

import org.main.additionalservices.AdditionalService;
import org.main.menu.Command;
import org.main.order.Order;
import org.main.utils.AdditionalServiceUtils;

import java.util.List;

public class ChooseAdditionalServiceCommand implements Command {
    private final Order order;

    public ChooseAdditionalServiceCommand(Order order) {
        this.order = order;
    }

    @Override
    public String getKey() {
        return "choose_additional_service";
    }

    @Override
    public String getParameters() {
        return "[name]";
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() == 1) {
            AdditionalService additionalService = AdditionalServiceUtils.getAdditionalServiceByName(parameters.get(0));
            if (additionalService != null) {
                this.order.setAdditionalService(additionalService);
                System.out.println("Additional Service chosen successfully");
            }
        } else {
            System.out.println("Wrong input");
        }
    }
}
