package org.main.menu.submenu;

import org.main.company.Company;
import org.main.menu.Command;
import org.main.order.Order;
import org.main.utils.ApplicationLogger;

import java.util.List;

public class CompleteOrderCommand implements Command {
    private final Company company;
    private final Order order;

    public CompleteOrderCommand(Company company, Order order) {
        this.company = company;
        this.order = order;
    }

    @Override
    public String getKey(){
        return "complete";
    }

    @Override
    public void execute(List<String> params) {
        ApplicationLogger.logInfo(this.getClass(), "Order " + order.hashCode() + " was completed");
        company.addOrder(order);
        System.out.println("Order completed!");
    }


}
