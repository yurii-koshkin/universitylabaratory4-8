package org.main.menu.submenu;

import org.main.menu.Command;
import org.main.order.Order;

import java.util.List;

public class RemoveCallsCommand implements Command {
    private final Order order;

    public RemoveCallsCommand(Order order) {
        this.order = order;
    }

    @Override
    public String getKey() {
        return "remove_call_minutes";
    }

    @Override
    public String getParameters() {
        return "[minutes]";
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() == 1) {
            if (this.order.getTariff().removeCallMinutes(Integer.parseInt(parameters.get(0)))) {
                System.out.println(parameters.get(0) + " call minutes was successfully removed from your tariff");
            } else {
                System.out.println("There are lower than " + parameters.get(0) + " additional minutes");
            }
        }
    }

}
