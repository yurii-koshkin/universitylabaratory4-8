package org.main.menu.submenu;

import org.main.company.Company;
import org.main.menu.Command;
import org.main.order.Order;
import org.main.tariff.Tariff;

import java.util.List;

public class ChooseTariffCommand implements Command {
    private final Company company;
    private final Order order;

    public ChooseTariffCommand(Company company, Order order) {
        this.company = company;
        this.order = order;
    }

    @Override
    public String getKey() {
        return "choose_tariff";
    }

    @Override
    public String getParameters() {
        return "[name]";
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() == 1) {
            Tariff tariff = company.getTariffs().get(parameters.get(0));
            if (tariff != null) {
                this.order.setTariff(tariff);
                System.out.println("Tariff chosen successfully");
            } else {
                System.out.println("This Tariff does not exist");
            }
        } else {
            System.out.println("Wrong input");
        }
    }
}
