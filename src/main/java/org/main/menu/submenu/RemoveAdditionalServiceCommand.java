package org.main.menu.submenu;

import org.main.menu.Command;
import org.main.order.Order;

import java.util.List;

public class RemoveAdditionalServiceCommand implements Command {
    private final Order order;

    public RemoveAdditionalServiceCommand(Order order) {
        this.order = order;
    }

    @Override
    public String getKey() {
        return "remove_additional_service";
    }

    @Override
    public void execute(List<String> parameters) {
        this.order.setAdditionalService(null);
        System.out.println("Additional Service removed successfully");
    }
}
