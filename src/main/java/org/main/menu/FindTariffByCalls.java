package org.main.menu;

import org.main.company.Company;

import java.util.List;

public class FindTariffByCalls implements Command {
    private final Company company;

    public FindTariffByCalls(Company company) {
        this.company = company;
    }

    @Override
    public String getKey() {
        return "find_tariff_by_calls";
    }

    @Override
    public String getParameters() {
        return "[min call minutes, max call minutes]";
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() == 2) {
            int minCallMinutes = Integer.parseInt(parameters.get(0));
            int maxCallMinutes = Integer.parseInt(parameters.get(1));
            company.getTariffs().values().stream()
                    .filter(t -> t.getCallMinutes() >= minCallMinutes && t.getInternetGigabytes() <= maxCallMinutes)
                    .forEach(t -> System.out.println("\n" + t));
        } else {
            System.out.println("Wrong input");
        }
    }
}
