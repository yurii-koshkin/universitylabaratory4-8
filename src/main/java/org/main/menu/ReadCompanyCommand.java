package org.main.menu;

import org.main.company.Company;
import org.main.utils.WriterReaderUtils;

import java.util.List;

public class ReadCompanyCommand implements Command {
    private final Company company;

    public ReadCompanyCommand(Company company) {
        this.company = company;
    }

    @Override
    public String getKey() {
        return "read_company";
    }

    @Override
    public String getParameters() {
        return "[file name]";
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() > 0) {
            this.company.importCompany(WriterReaderUtils.importCompany(String.join(" ", parameters)));
            System.out.println("Company was successfully imported!");
        } else {
            System.out.println("Wrong Input!");
        }
    }

}
