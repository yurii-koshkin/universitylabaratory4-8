package org.main.menu;

import org.main.company.Company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Menu {
    protected Map<String, Command> commands = new HashMap<>();
    private final List<String> help = new ArrayList<>();
    private final Company company = new Company();

    public Menu() {
        initMenu();
        initHelp();
    }

    protected void initMenu() {
        Command command = new OpenSubMenuCommand(company);
        commands.put(command.getKey(), command);

        command = new FindTariffByCalls(company);
        commands.put(command.getKey(), command);

        command = new FindTariffByInternet(company);
        commands.put(command.getKey(), command);

        command = new PrintCustomersCountCommand(company);
        commands.put(command.getKey(), command);

        command = new ExitCommand();
        commands.put(command.getKey(), command);

        command = new PrintTariffsCommand(company);
        commands.put(command.getKey(), command);

        command = new SaveCompanyCommand(company);
        commands.put(command.getKey(), command);

        command = new ReadCompanyCommand(company);
        commands.put(command.getKey(), command);

        command = new PrintOrdersCommand(company);
        commands.put(command.getKey(), command);
    }

    private void initHelp() {
        commands.forEach((k, v) -> help.add(k + (!v.getParameters().isEmpty() ? " " + v.getParameters() : "")));
    }

    public void execute(List<String> commandWithParams) {
        String command = commandWithParams.remove(0);

        if (commands.containsKey(command)) {
            commands.get(command).execute(commandWithParams);
        } else if (command.equals("help")) {
            System.out.println("\n" + help + "\n");
        } else {
            System.out.println("Command not exist, try \"Help\"");
        }

        commandWithParams.add(command);
    }

}
