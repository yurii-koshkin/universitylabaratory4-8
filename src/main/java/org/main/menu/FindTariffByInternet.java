package org.main.menu;

import org.main.company.Company;

import java.util.List;

public class FindTariffByInternet implements Command {
    private final Company company;

    public FindTariffByInternet(Company company) {
        this.company = company;
    }

    @Override
    public String getKey() {
        return "find_tariff_by_internet";
    }

    @Override
    public String getParameters() {
        return "[min gigabytes, max gigabytes]";
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() == 2) {
            double minGigabytes = Double.parseDouble(parameters.get(0));
            double maxGigabytes = Double.parseDouble(parameters.get(1));
            company.getTariffs().values().stream()
                    .filter(t -> t.getInternetGigabytes() >= minGigabytes && t.getInternetGigabytes() <= maxGigabytes)
                    .forEach(t -> System.out.println("\n" + t));
        } else {
            System.out.println("Wrong input");
        }
    }
}
