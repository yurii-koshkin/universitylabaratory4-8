package org.main.menu;

import org.main.company.Company;
import org.main.tariff.Tariff;

import java.util.Comparator;
import java.util.List;

public class PrintTariffsCommand implements Command {
    private final Company company;

    public PrintTariffsCommand(Company company) {
        this.company = company;
    }

    @Override
    public String getKey() {
        return "print_tariffs";
    }

    @Override
    public void execute(List<String> parameters) {
        company.getTariffs().values().stream().sorted(Comparator.comparingDouble(Tariff::getPrice))
                .forEach(t -> System.out.println("\n" + t));
    }

}
