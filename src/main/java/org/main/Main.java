package org.main;

import org.apache.log4j.PropertyConfigurator;
import org.main.menu.Menu;
import org.main.utils.ApplicationLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        PropertyConfigurator.configure("log4j.properties");
//        ApplicationLogger.logFatal("test");

        List<String> commandWithParams;
        Scanner scanner = new Scanner(System.in);
        Menu mainMenu = new Menu();

        do {
            System.out.print("\nInput your command: ");
            commandWithParams = new ArrayList<>(Arrays.asList(scanner.nextLine().split(" ")));

            mainMenu.execute(commandWithParams);
        } while (!commandWithParams.get(0).equals("exit"));
    }

}