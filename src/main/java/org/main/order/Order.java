package org.main.order;

import org.main.additionalservices.AdditionalService;
import org.main.tariff.Tariff;

import java.io.Serializable;
import java.util.Date;

public class Order implements Serializable {
    private static final long  SerialVersionUID = 5L;

    private Tariff tariff;
    private AdditionalService additionalService;
    private String addServiceName;
    private Date startDate;
    private Date endDate;

    public double getPrice() {
        return (tariff != null ? tariff.getPrice() : 0) + (additionalService != null ? additionalService.getPrice() : 0);
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    public AdditionalService getAdditionalService() {
        return additionalService;
    }

    public void setAdditionalService(AdditionalService additionalService) {
        this.additionalService = additionalService;
    }

    public String getAddServiceName() {
        return addServiceName;
    }

    public void setAddServiceName(String addServiceName) {
        this.addServiceName = addServiceName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return String.format(
                "Tariff: %s\n\nAdditional Service: %s\n\nTotal Price: %f",
                tariff != null ? tariff : "none",
                additionalService != null? "\n" + additionalService : "none",
                this.getPrice()
        );
    }
}
