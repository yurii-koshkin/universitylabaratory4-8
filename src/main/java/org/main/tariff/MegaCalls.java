package org.main.tariff;

import java.io.Serializable;

public class MegaCalls extends Tariff implements Serializable {
    private static final long  SerialVersionUID = 9L;

    public MegaCalls() {
        super.internetGigabytes = 5;
        super.callMinutes = 1200;
        super.price = 200;
    }

    @Override
    public String toString() {
        return "Mega Calls\n" + super.toString();
    }

}
