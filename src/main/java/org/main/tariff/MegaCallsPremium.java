package org.main.tariff;

import java.io.Serializable;

public class MegaCallsPremium extends MegaCalls implements Serializable {
    private static final long  SerialVersionUID = 10L;

    public MegaCallsPremium() {
        super.internetGigabytes = 10;
        super.callMinutes = 1800;
        super.price = 300;
    }

    @Override
    public String toString() {
        return "Premium " + super.toString();
    }

}
