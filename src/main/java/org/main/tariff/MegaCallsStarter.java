package org.main.tariff;

import java.io.Serializable;

public class MegaCallsStarter extends MegaCalls implements Serializable {
    private static final long  SerialVersionUID = 11L;

    public MegaCallsStarter() {
        super.internetGigabytes = 0.5;
        super.callMinutes = 600;
        super.price = 100;
    }

    @Override
    public String toString() {
        return "Starter " + super.toString();
    }

}
