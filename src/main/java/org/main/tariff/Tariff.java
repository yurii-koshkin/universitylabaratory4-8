package org.main.tariff;

import java.io.Serializable;

public abstract class Tariff implements Serializable {
    private static final long  SerialVersionUID = 12L;

    protected double internetGigabytes;
    protected int callMinutes;
    protected double price;

    private double additionalInternetGigabytes;
    private int additionalCallMinutes;
    private final double additionalCallMinutePrice = 1;
    private final double additionalInternetGigabytePrice = 50;

    private String name;

    public void addCallMinutes(int minutes) {
        this.additionalCallMinutes += minutes;
        this.price += minutes * additionalCallMinutePrice;
    }

    public boolean removeCallMinutes(int minutes) {
        if (this.additionalCallMinutes >= minutes) {
            this.additionalCallMinutes -= minutes;
            this.price -= minutes * additionalCallMinutePrice;

            return true;
        } else {
            return false;
        }
    }

    public void addInternetGigabytes(double gigabytes) {
        this.additionalInternetGigabytes += gigabytes;
        this.price += gigabytes * additionalInternetGigabytePrice;
    }

    public boolean removeInternetGigabytes(double gigabytes) {
        if (this.additionalInternetGigabytes >= gigabytes) {
            this.additionalInternetGigabytes -= gigabytes;
            this.price -= gigabytes * additionalInternetGigabytePrice;

            return true;
        } else {
            return false;
        }
    }

    public double getInternetGigabytes() {
        return internetGigabytes;
    }

    public int getCallMinutes() {
        return callMinutes;
    }

    public double getAdditionalInternetGigabytes() {
        return additionalInternetGigabytes;
    }

    public int getAdditionalCallMinutes() {
        return additionalCallMinutes;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format(
            "Mobile Internet: %.2f gb + %.2f additional gb\nInternational Call minutes: %d + %d additional\nPrice: %.2f",
            this.internetGigabytes, this.additionalInternetGigabytes, this.callMinutes, this.additionalCallMinutes, this.price
        );
    }

}
