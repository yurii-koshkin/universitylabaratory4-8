package org.main.tariff;

import java.io.Serializable;

public class InternetAddictedStarter extends InternetAddicted implements Serializable {
    private static final long  SerialVersionUID = 8L;

    public InternetAddictedStarter() {
        super.internetGigabytes = 25;
        super.callMinutes = 0;
        super.price = 100;
    }

    @Override
    public String toString() {
        return "Starter " + super.toString();
    }

}
