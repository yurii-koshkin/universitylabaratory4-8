package org.main.tariff;

import java.io.Serializable;

public class InternetAddictedPremium extends InternetAddicted implements Serializable {
    private static final long  SerialVersionUID = 7L;

    public InternetAddictedPremium() {
        super.internetGigabytes = 100;
        super.callMinutes = 120;
        super.price = 300;
    }

    @Override
    public String toString() {
        return "Premium " + super.toString();
    }

}
