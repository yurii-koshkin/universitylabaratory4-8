package org.main.tariff;

import java.io.Serializable;

public class InternetAddicted extends Tariff implements Serializable {
    private static final long  SerialVersionUID = 6L;

    public InternetAddicted() {
        super.internetGigabytes = 50;
        super.callMinutes = 60;
        super.price = 200;
    }

    @Override
    public String toString() {
        return "Internet Addicted\n" + super.toString();
    }

}
