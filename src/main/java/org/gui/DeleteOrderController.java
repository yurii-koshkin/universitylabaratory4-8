package org.gui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.database.DBController;

import java.io.IOException;
import java.util.Map;

public class DeleteOrderController {
    @FXML
    public Button deleteButton;

    @FXML
    public Button backButton;

    @FXML
    private Label successMessage;

    @FXML
    public ComboBox<String> orderList;

    public void initialize() {
        Map<String, Integer> orders = DBController.getOrders();
        orderList.getItems().setAll(orders.keySet());
        orderList.getSelectionModel().selectFirst();

        backButton.setOnAction(event -> {
            backButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MainMenu.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Stage stage = new Stage();
            stage.setScene(new Scene(loader.getRoot()));
            stage.setTitle("Курсова робота Кошкіна Юрія КН-207");
            stage.show();
        });

        deleteButton.setOnAction(actionEvent -> {
            DBController.deleteOrder(orders.get(orderList.getSelectionModel().getSelectedItem()));
            successMessage.setText("Замовлення " + orderList.getSelectionModel().getSelectedItem() + " видалено!");

            Map<String, Integer> refreshedOrders = DBController.getOrders();
            orderList.getItems().setAll(refreshedOrders.keySet());
            orderList.getSelectionModel().selectFirst();
        });
    }
}
