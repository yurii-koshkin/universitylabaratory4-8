package org.gui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.database.DBController;

import java.io.IOException;

public class CreateCompanyController {

    @FXML
    public Button createButton;

    @FXML
    public Button backButton;

    @FXML
    private Label successMessage;

    @FXML
    private TextField nameField;


    public void initialize() {
        createButton.setOnAction(event -> {
            DBController.createCompany(nameField.getText());
            successMessage.setText("Компанію " + nameField.getText() + " створено!");
        });

        backButton.setOnAction(event -> {
            backButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MainMenu.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Stage stage = new Stage();
            stage.setScene(new Scene(loader.getRoot()));
            stage.setTitle("Курсова робота Кошкіна Юрія КН-207");
            stage.show();
        });
    }

}
