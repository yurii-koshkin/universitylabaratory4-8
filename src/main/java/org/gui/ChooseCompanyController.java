package org.gui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.database.DBController;

import java.io.IOException;
import java.util.Map;

public class ChooseCompanyController {
    @FXML
    public Button chooseButton;

    @FXML
    public Button backButton;

    @FXML
    private Label successMessage;

    @FXML
    public ComboBox<String> companyList;

    public void initialize() {
        Map<String, Integer> companies = DBController.getCompanies();
        companyList.getItems().setAll(companies.keySet());
        companyList.getSelectionModel().selectFirst();

        backButton.setOnAction(event -> {
            backButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MainMenu.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Stage stage = new Stage();
            stage.setScene(new Scene(loader.getRoot()));
            stage.setTitle("Курсова робота Кошкіна Юрія КН-207");
            stage.show();
        });

        chooseButton.setOnAction(actionEvent -> {
            CompanyIdReference.setCompanyId(companies.get(companyList.getSelectionModel().getSelectedItem()));
            successMessage.setText("Компанію " + companyList.getSelectionModel().getSelectedItem() + " обрано!");
        });
    }

}
