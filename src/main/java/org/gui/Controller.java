package org.gui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button createOrderButton;

    @FXML
    private Button deleteOrderButton;

    @FXML
    private Button exitButton;

    @FXML
    private Button chooseCompanyButton;

    @FXML
    private Button createCompanyButton;

    @FXML
    public Button deleteCompanyButton;

    @FXML
    public Button viewOrdersButton;

    private static int company_id;

    @FXML
    public void initialize() {
        createCompanyButton.setOnAction(event -> {
            createCompanyButton.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();

            try {
                loader.setLocation(getClass().getResource("CreateCompany.fxml"));
                loader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Stage stage = new Stage();
            stage.setScene(new Scene(loader.getRoot()));
            stage.setTitle("Курсова робота Кошкіна Юрія КН-207");
            stage.show();
        });

        chooseCompanyButton.setOnAction(event -> {
            chooseCompanyButton.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();

            try {
                loader.setLocation(getClass().getResource("ChooseCompany.fxml"));
                loader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Stage stage = new Stage();
            stage.setScene(new Scene(loader.getRoot()));
            stage.setTitle("Курсова робота Кошкіна Юрія КН-207");
            stage.show();
        });

        deleteCompanyButton.setOnAction(event -> {
            deleteCompanyButton.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();

            try {
                loader.setLocation(getClass().getResource("DeleteCompany.fxml"));
                loader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Stage stage = new Stage();
            stage.setScene(new Scene(loader.getRoot()));
            stage.setTitle("Курсова робота Кошкіна Юрія КН-207");
            stage.show();
        });

        createOrderButton.setOnAction(event -> {
            createOrderButton.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();

            try {
                loader.setLocation(getClass().getResource("CreateOrder.fxml"));
                loader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Stage stage = new Stage();
            stage.setScene(new Scene(loader.getRoot()));
            stage.setTitle("Курсова робота Кошкіна Юрія КН-207");
            stage.show();
        });

        deleteOrderButton.setOnAction(event -> {
            deleteCompanyButton.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();

            try {
                loader.setLocation(getClass().getResource("DeleteOrder.fxml"));
                loader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Stage stage = new Stage();
            stage.setScene(new Scene(loader.getRoot()));
            stage.setTitle("Курсова робота Кошкіна Юрія КН-207");
            stage.show();
        });

        viewOrdersButton.setOnAction(event -> {
            viewOrdersButton.getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();

            try {
                loader.setLocation(getClass().getResource("ViewOrders.fxml"));
                loader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Stage stage = new Stage();
            stage.setScene(new Scene(loader.getRoot()));
            stage.setTitle("Курсова робота Кошкіна Юрія КН-207");
            stage.show();
        });

        exitButton.setOnAction(actionEvent -> {
            exitButton.getScene().getWindow().hide();
            System.exit(0);
        });
    }

}