package org.gui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.database.DBController;
import org.main.company.Company;
import org.main.order.Order;
import org.main.tariff.Tariff;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class CreateOrderController {
    @FXML
    public Button createButton;

    @FXML
    public Button backButton;

    @FXML
    private Label successMessage;

    @FXML
    public ComboBox<String> tariffList;

    @FXML
    public ComboBox<String> addServiceList;

    @FXML
    public TextField addInternetField;

    @FXML
    public TextField addCallsField;

    public void initialize() {
        Map<String, Tariff> tariffs = new Company().getTariffs();
        tariffList.getItems().setAll(tariffs.keySet());
        tariffList.getSelectionModel().selectFirst();

        addServiceList.getItems().setAll(Arrays.asList("Відсутній", "Домашній Інтернет", "Домашнє ТВ"));
        addServiceList.getSelectionModel().selectFirst();

        backButton.setOnAction(event -> {
            backButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MainMenu.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Stage stage = new Stage();
            stage.setScene(new Scene(loader.getRoot()));
            stage.setTitle("Курсова робота Кошкіна Юрія КН-207");
            stage.show();
        });

        createButton.setOnAction(actionEvent -> {
            Order order = new Order();
            order.setTariff(tariffs.get(tariffList.getSelectionModel().getSelectedItem()));
            order.getTariff().addCallMinutes(Integer.parseInt(addCallsField.getText()));
            order.getTariff().addInternetGigabytes(Double.parseDouble(addInternetField.getText()));
            order.setAddServiceName(addServiceList.getSelectionModel().getSelectedItem());
            order.getTariff().setName(tariffList.getSelectionModel().getSelectedItem());
            order.setStartDate(new Date());

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, 28);

            order.setEndDate(cal.getTime());

            DBController.createOrder(order);

            successMessage.setText("Створено!");
        });
    }
}
