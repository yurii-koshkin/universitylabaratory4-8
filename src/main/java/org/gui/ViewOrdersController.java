package org.gui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import org.database.DBController;
import org.main.order.Order;

import java.io.IOException;
import java.util.Map;

public class ViewOrdersController {

    @FXML
    public Button chooseButton;

    @FXML
    public Button backButton;

    @FXML
    public ComboBox<String> orderList;

    @FXML
    public TextArea detailText;

    public void initialize() {
        Map<String, Order> orders = DBController.getDetailOrders();
        orderList.getItems().setAll(orders.keySet());
        orderList.getSelectionModel().selectFirst();

        backButton.setOnAction(event -> {
            backButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("MainMenu.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Stage stage = new Stage();
            stage.setScene(new Scene(loader.getRoot()));
            stage.setTitle("Курсова робота Кошкіна Юрія КН-207");
            stage.show();
        });

        chooseButton.setOnAction(actionEvent -> {
            detailText.clear();
            Order selectedOrder = orders.get(orderList.getSelectionModel().getSelectedItem());

            detailText.appendText("Назва тарифу: " + orderList.getSelectionModel().getSelectedItem());
            detailText.appendText("\nДодатковий сервіс: " + selectedOrder.getAddServiceName());
            detailText.appendText("\nІнтернет GB: " + selectedOrder.getTariff().getInternetGigabytes());
            detailText.appendText("\nДодатковий Інтернет GB: " + selectedOrder.getTariff().getAdditionalInternetGigabytes());
            detailText.appendText("\nДзвінки хвилини: " + selectedOrder.getTariff().getCallMinutes());
            detailText.appendText("\nДодаткові Дзвінки хвилини: " + selectedOrder.getTariff().getAdditionalCallMinutes());
            detailText.appendText("\nЦіна: " + selectedOrder.getPrice());
            detailText.appendText("\nПочаток дії: " + selectedOrder.getStartDate());
            detailText.appendText("\nКінець дії: " + selectedOrder.getEndDate());
        });
    }

}
