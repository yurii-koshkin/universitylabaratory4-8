package org.gui;

public class CompanyIdReference {
    private static int companyId;

    public static int getCompanyId() {
        return companyId;
    }

    public static void setCompanyId(int id) {
        companyId = id;
    }
}
