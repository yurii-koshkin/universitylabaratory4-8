package org.main.menu;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class MenuGlobalTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void restoreStreams(){
        System.setOut(new PrintStream(originalOut));
    }

    @Test
    public void chooseTariffCommandTest() {
        System.setIn(new ByteArrayInputStream(("choose_tariff internet_addicted_premium\ncomplete\n").getBytes()));

        Menu menu = new Menu();
        menu.execute(new ArrayList<>(List.of("create_order")));
        menu.execute(new ArrayList<>(List.of("print_orders")));

        Assertions.assertTrue(outContent.toString().contains("Premium Internet Addicted"));
    }

    @Test
    public void chooseAdditionalServiceCommandTest() {
        System.setIn(new ByteArrayInputStream(("choose_additional_service home_tv\ncomplete\n").getBytes()));

        Menu menu = new Menu();
        menu.execute(new ArrayList<>(List.of("create_order")));
        menu.execute(new ArrayList<>(List.of("print_orders")));

        Assertions.assertTrue(outContent.toString().contains("Home TV"));
    }

    @Test
    public void addCallsCommandTest() {
        System.setIn(new ByteArrayInputStream(("add_call_minutes 100\ncomplete\n").getBytes()));

        Menu menu = new Menu();
        menu.execute(new ArrayList<>(List.of("create_order")));
        menu.execute(new ArrayList<>(List.of("print_orders")));

        Assertions.assertTrue(outContent.toString().contains("You should firstly choose tariff"));

        System.setIn(new ByteArrayInputStream(("choose_tariff mega_calls_premium\nadd_call_minutes 100\ncomplete\n").getBytes()));

        menu.execute(new ArrayList<>(List.of("create_order")));
        menu.execute(new ArrayList<>(List.of("print_orders")));
        Assertions.assertTrue(outContent.toString().contains("100 additional"));
    }

    @Test
    public void removeCallsCommandTest() {
        System.setIn(new ByteArrayInputStream(("choose_tariff mega_calls\nadd_call_minutes 200\nremove_call_minutes 150\ncomplete\n").getBytes()));

        Menu menu = new Menu();
        menu.execute(new ArrayList<>(List.of("create_order")));
        menu.execute(new ArrayList<>(List.of("print_orders")));

        Assertions.assertTrue(outContent.toString().contains("50 additional"));
    }

    @Test
    public void addGigabytesCommandTest() {
        System.setIn(new ByteArrayInputStream(("add_gigabytes 100\ncomplete\n").getBytes()));

        Menu menu = new Menu();
        menu.execute(new ArrayList<>(List.of("create_order")));
        menu.execute(new ArrayList<>(List.of("print_orders")));

        Assertions.assertTrue(outContent.toString().contains("You should firstly choose tariff"));

        System.setIn(new ByteArrayInputStream(("choose_tariff internet_addicted\nadd_gigabytes 100\ncomplete\n").getBytes()));

        menu.execute(new ArrayList<>(List.of("create_order")));
        menu.execute(new ArrayList<>(List.of("print_orders")));
        Assertions.assertTrue(outContent.toString().contains("100,00 additional"));
    }

    @Test
    public void removeGigabytesCommandTest() {
        System.setIn(new ByteArrayInputStream(("choose_tariff mega_calls\nadd_gigabytes 100\nremove_gigabytes 150\ncomplete\n").getBytes()));

        Menu menu = new Menu();
        menu.execute(new ArrayList<>(List.of("create_order")));
        menu.execute(new ArrayList<>(List.of("print_orders")));

        Assertions.assertTrue(outContent.toString().contains("There are lower than"));
    }

    @Test
    public void removeAdditionalServiceCommandTest() {
        System.setIn(new ByteArrayInputStream(("choose_additional_service home_internet\ncomplete\n").getBytes()));

        Menu menu = new Menu();
        menu.execute(new ArrayList<>(List.of("create_order")));
        menu.execute(new ArrayList<>(List.of("print_orders")));
        Assertions.assertTrue(outContent.toString().contains("Home Internet"));

        System.setIn(new ByteArrayInputStream(("choose_additional_service home_internet\nremove_additional_service home_internet\ncomplete\n").getBytes()));

        menu.execute(new ArrayList<>(List.of("create_order")));
        menu.execute(new ArrayList<>(List.of("print_orders")));

        Assertions.assertTrue(outContent.toString().contains("Additional Service: none"));
    }

    @Test
    public void printOrderCommandTest() {
        System.setIn(new ByteArrayInputStream(("choose_tariff mega_calls_starter\nprint_order\ncomplete\n").getBytes()));

        Menu menu = new Menu();
        menu.execute(new ArrayList<>(List.of("create_order")));

        Assertions.assertTrue(outContent.toString().contains("Starter Mega Calls"));
    }

    @Test
    public void printCustomersCountCommandTest() {
        System.setIn(new ByteArrayInputStream(("choose_tariff mega_calls_starter\nprint_order\ncomplete\n").getBytes()));

        Menu menu = new Menu();
        menu.execute(new ArrayList<>(List.of("create_order")));
        menu.execute(new ArrayList<>(List.of("print_customers_count")));

        Assertions.assertTrue(outContent.toString().contains("1"));
    }

    @Test
    public void findTariffByCallsCommandTest() {
        Menu menu = new Menu();
        menu.execute(new ArrayList<>(List.of("find_tariff_by_calls", "10", "200")));
        Assertions.assertTrue(outContent.toString().contains("Starter Mega Calls"));
    }

    @Test
    public void findTariffByInternetCommandTest() {
        Menu menu = new Menu();
        menu.execute(new ArrayList<>(List.of("find_tariff_by_internet", "10", "200")));
        Assertions.assertTrue(outContent.toString().contains("Starter Internet Addicted"));
    }

    @Test
    public void printTariffsCommandTest() {
        Menu menu = new Menu();
        menu.execute(new ArrayList<>(List.of("print_tariffs")));
        Assertions.assertTrue(outContent.toString().contains("Starter Internet Addicted"));
    }

}
